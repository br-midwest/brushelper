PROGRAM _CYCLIC
	(* Returns the average value OF elements in a 1dimensional array. *)
	AvgArr(ADR(array), brusDATATYPE_INT, SIZEOF(array) / SIZEOF(array[0]));
	
END_PROGRAM

