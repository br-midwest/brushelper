
PROGRAM _CYCLIC
	(* Generates a symmetric on/off square wave with configurable period. *)
	BRUS_Blink_0(Enable := blinkEnable, Period := blinkCycle, Update := blinkUpdate);
END_PROGRAM
