

PROGRAM _CYCLIC
	(* Produces a linear regression model *)
	 BRUS_LinReg_0(Execute := executeBool, pX := ADR(xArray), pY := ADR(yArray), n := numDataPoint);
END_PROGRAM

