
PROGRAM _CYCLIC
	(* Detects multiple click states for a single button input with configurable input window *)
	BRUS_MultiClick_0(Enable := enableMultiClick, InputSource := inputBool , InputWindow := inputWindow, Update := updateMultiClick);
	
END_PROGRAM
