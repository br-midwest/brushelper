
PROGRAM _CYCLIC
	(* Keeps time that machine is in a certain state *)
	BRUS_Runtime_0(Enable := enableRuntime, pRuntimeCum := ADR(runtime), RunCondition := runCondition, TimeUnit := timeDiv, Reset := resetRuntime, Update := updateRuntime);

END_PROGRAM
