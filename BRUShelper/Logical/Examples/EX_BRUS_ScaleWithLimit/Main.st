
PROGRAM _CYCLIC
	(* Scales an Analog input to the specified range *)
	BRUS_ScaleWithLimit_0(Enable := enableScale, In := realIn, InMin := realInMin, InMax := realInMax, OutMin := realOutMin, OutMax := realOutMax, LowWarnVal := lowWarnValue, HighWarnVal := highWarnValue, LimitRange := limitBool, Update := updateScale);
	
END_PROGRAM
