
PROGRAM _CYCLIC
	(* Converts numeric strings between bases *)
	inputString := UINT_TO_STRING(inputBase);
	status := Base2Base(inputBase, outputBase, ADR(inputString),ADR(outputString));
END_PROGRAM
