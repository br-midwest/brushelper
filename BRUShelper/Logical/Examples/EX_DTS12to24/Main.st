
PROGRAM _CYCLIC
	
	(* Converts 12 hour time DTStructure back to 24 hour time DTStructure. *)
	DTS12to24(ADR(OneTwoHour), ADR(TwoFourHour), isPM);
	
END_PROGRAM

