
PROGRAM _CYCLIC
	(* Converts 24 hour time DT structure into 12 hour time DT structure. Returns TRUE for PM. *)
	amORpm := DTS24to12(ADR(TwoFourHour), ADR(OneTwoHour));
	
END_PROGRAM
