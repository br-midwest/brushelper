

PROGRAM _CYCLIC
	
	(* Functional increment function, returns true for 1 cycle when the value is incremented in the appropriate direction *)
	IF EDGEPOS(run) THEN
		
		incOrDecBool := IncDecCheck(enableBool, processVar, ADR(previousVal), directionBool);
		
	END_IF
	
END_PROGRAM

