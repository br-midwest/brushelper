
PROGRAM _CYCLIC
	
	(* Finds if two reals equal to one another within a range *)
	withinBool := WithinReal(real1,real2,realTolerance);
	
END_PROGRAM

