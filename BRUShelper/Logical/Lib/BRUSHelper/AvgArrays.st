(********************************************************
 * Copyright: Bernecker + Rainer
*********************************************************
 * Author:    Brad Jones
 * Created:   January 16, 2017/3:55 PM 
*********************************************************
[PROGRAM INFORMATION]
Returns the average value OF elements in a 1dimensional 
array.
********************************************************)

(*for floating point numbers*)
FUNCTION AvgArr
	
	// sum the elements
	FOR i := 0 TO numElements - 1 DO
		CASE datatype OF
			brusDATATYPE_SINT:		
				brsmemmove(ADR(oneOfEach.mySINT), pArr + (i*1), 1);
				sum	:= sum + SINT_TO_LREAL(oneOfEach.mySINT);
			
			brusDATATYPE_USINT:		
				brsmemmove(ADR(oneOfEach.myUSINT), pArr + (i*1), 1);
				sum	:= sum + USINT_TO_LREAL(oneOfEach.myUSINT);
			
			brusDATATYPE_INT:		
				brsmemmove(ADR(oneOfEach.myINT), pArr + (i*2), 2);
				sum	:= sum + INT_TO_LREAL(oneOfEach.myINT);
			
			brusDATATYPE_UINT:		
				brsmemmove(ADR(oneOfEach.myUINT), pArr + (i*2), 2);
				sum	:= sum + UINT_TO_LREAL(oneOfEach.myUINT);
			
			brusDATATYPE_DINT:		
				brsmemmove(ADR(oneOfEach.myDINT), pArr + (i*4), 4);
				sum	:= sum + DINT_TO_LREAL(oneOfEach.myDINT);
			
			brusDATATYPE_UDINT:		
				brsmemmove(ADR(oneOfEach.myUDINT), pArr + (i*4), 4);
				sum	:= sum + UDINT_TO_LREAL(oneOfEach.myUDINT);
			
			brusDATATYPE_REAL:		
				brsmemmove(ADR(oneOfEach.myREAL), pArr + (i*4), 4);
				sum	:= sum + REAL_TO_LREAL(oneOfEach.myREAL);
			
			brusDATATYPE_LREAL:		
				brsmemmove(ADR(oneOfEach.myLREAL), pArr + (i*8), 8);
				sum	:= sum + (oneOfEach.myLREAL);
		END_CASE;
	END_FOR
	
	// return average
	AvgArr	:= LREAL_TO_REAL(sum / numElements);
	
END_FUNCTION