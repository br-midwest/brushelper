
FUNCTION lrealpow : LREAL
	VAR_INPUT
		base : UINT; (* *) (* *) (*#PAR*)
		exponent : UINT; (* *) (* *) (*#PAR*)
	END_VAR
	VAR
		i : INT; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION_BLOCK BRUS_Runtime (*Keeps track of time that machine spends in a certain state*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Enable : BOOL; (*Enables the function*) (* *) (*#PAR*)
		pRuntimeCum : UDINT; (*Address of permanent variable which is used to keep track of the cumulative run time*) (* *) (*#PAR*)
		RunCondition : BOOL; (*Logical condition/state for which the timer will count up*) (* *) (*#CMD*)
		TimeUnit : BRUS_Runtime_TimeUnit_enum; (*Largest division of time to display in output struct*) (* *) (*#PAR*)
		Reset : BOOL; (*Reset the timer*) (* *) (*#PAR*)
		Update : BOOL; (*Updates the parameter inputs*) (* *) (*#PAR*)
	END_VAR
	VAR_OUTPUT
		Active : BOOL; (*True while function block is operating normally*) (* *) (*#PAR*)
		Error : BOOL; (*An error has occured, see statusID for more details*) (* *) (*#PAR*)
		StatusID : BRUS_ID_enum; (*status number*) (* *) (*#PAR*)
		Runtime : BRUS_Runtime_Times_typ; (*Output runtime in configured format*) (* *) (*#PAR*)
	END_VAR
	VAR
		IS : BRUS_Runtime_IS; (* *) (* *) (*#OMIT*)
		RuntimeCum : REFERENCE TO UDINT; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK BRUS_MultiClick (*Detects multiple click states for a button input with configurable input window*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Enable : BOOL; (*Enables the function*) (* *) (*#CMD*)
		InputSource : BOOL; (*Source of the clicks*) (* *) (*#PAR*)
		InputWindow : UINT; (*Input window to accept clicks (milliseconds)*) (* *) (*#PAR*)
		Update : BOOL; (*Updates the parameter inputs*) (* *) (*#CMD*)
	END_VAR
	VAR_OUTPUT
		Active : BOOL; (*True while function block is operating normally*) (* *) (*#PAR*)
		Error : BOOL; (*An error has occured, see statusID for more details*) (* *) (*#PAR*)
		StatusID : BRUS_ID_enum; (*status number*) (* *) (*#PAR*)
		Single : BOOL; (*TRUE for one cycle if single click detected*) (* *) (*#PAR*)
		Double : BOOL; (*TRUE for one cycle if double click detected*) (* *) (*#PAR*)
		Triple : BOOL; (*TRUE for one cycle if triple click detected*) (* *) (*#PAR*)
		Long : BOOL; (*TRUE for one cycle if long click detected*) (* *) (*#PAR*)
	END_VAR
	VAR
		IS : BRUS_MultiClick_IS; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK BRUS_Blink (*Generates a symmetric on/off square wave with configurable period.*) (*$GROUP=User*)
	VAR_INPUT
		Enable : BOOL; (*Enables flashing output*) (* *) (*#CMD*)
		Period : TIME; (*Period of full cycle*) (* *) (*#PAR*)
		Update : BOOL; (* *) (* *) (*#PAR*)
	END_VAR
	VAR_OUTPUT
		Active : BOOL; (* *) (* *) (*#PAR*)
		Error : BOOL; (* *) (* *) (*#PAR*)
		StatusID : BRUS_ID_enum; (* *) (* *) (*#PAR*)
		Out : BOOL; (*Square wave out*) (* *) (*#PAR*)
	END_VAR
	VAR
		IS : BRUS_Blink_IS; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK BRUS_ScaleWithLimit (*Scales an Analog input to the specified range*)
	VAR_INPUT
		Enable : BOOL; (*Enables scaling*) (* *) (*#CMD*)
		In : REAL; (*Analog value to be scaled*) (* *) (*#PAR*)
		InMin : REAL; (*Expected minimum of analog value*) (* *) (*#PAR*)
		InMax : REAL; (*Expected Maximum of analog value*) (* *) (*#PAR*)
		OutMin : REAL; (*Output corresponding to minimum input*) (* *) (*#PAR*)
		OutMax : REAL; (*Output corresponding to maximum input*) (* *) (*#PAR*)
		LowWarnVal : REAL; (*Output low warning level*) (* *) (*#PAR*)
		HighWarnVal : REAL; (*Output high warning level*) (* *) (*#PAR*)
		LimitRange : BOOL; (*Enable to limit range to OutMax and OutMin*) (* *) (*#PAR*)
		Update : BOOL; (*Update parameters the function is using*) (* *) (*#CMD*)
	END_VAR
	VAR_OUTPUT
		Active : BOOL; (*True while function block is operating normally*) (* *) (*#PAR*)
		Out : REAL; (*Scaled output value*) (* *) (*#PAR*)
		Error : BOOL; (*True if InMax = InMin*) (* *) (*#PAR*)
		StatusID : BRUS_ID_enum; (*status number*) (* *) (*#PAR*)
		LowWarning : BOOL; (*True if Input < Low warning value*) (* *) (*#PAR*)
		OutOfRangeLow : BOOL; (*True if Input < InMin*) (* *) (*#PAR*)
		HighWarning : BOOL; (*True if Input > High warning value*) (* *) (*#PAR*)
		OutOfRangeHigh : BOOL; (*True if Input > InMax*)
	END_VAR
	VAR
		IS : BRUS_ScaleWithLimit_IS; (* *) (* *) (*#OMIT*)
		slope : REAL; (* *) (* *) (*#OMIT*)
		constant : REAL; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION DTS24to12 : BOOL (*Converts 24 hour time DT structure into 12 hour time DT structure. Returns TRUE for PM.*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		pDTStruct24Hr : UDINT; (*Address of DateTime structure to be converted*) (* *) (*#PAR*)
		pDTStruct12Hr : UDINT; (*Address of DateTime structure that will be filled with 12 hour time*) (* *) (*#PAR*)
	END_VAR
	VAR
		DT24 : REFERENCE TO DTStructure; (* *) (* *) (*#OMIT*)
		DT12 : REFERENCE TO DTStructure; (* *) (* *) (*#OMIT*)
		isPM : BOOL; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION DTS12to24 : BOOL (*Converts 12 hour time DTStructure back to 24 hour time DTStructure.*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		pDTStruct24Hr : UDINT; (*Address of DateTime structure to be converted*) (* *) (*#PAR*)
		pDTStruct12Hr : UDINT; (*Address of DateTime structure that will be filled with 24 hour time*) (* *) (*#PAR*)
		isPM : BOOL; (* *) (* *) (*#PAR*)
	END_VAR
	VAR
		DT12 : REFERENCE TO DTStructure; (* *) (* *) (*#OMIT*)
		DT24 : REFERENCE TO DTStructure; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION RoundReal : REAL (*Rounds a float to a given decimal place*) (*$GROUP=User*)
	VAR_INPUT
		Real1 : REAL; (*Value to Round*) (* *) (*#PAR*)
		Decimals : REAL; (*Number of Decimal Places*) (* *) (*#PAR*)
	END_VAR
	VAR
		Remainder : REAL; (* *) (* *) (*#OMIT*)
		TempREAL : REAL; (* *) (* *) (*#OMIT*)
		IntPart : DINT; (* *) (* *) (*#OMIT*)
		Test : REAL; (* *) (* *) (*#OMIT*)
		ExpTemp : REAL; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION WithinReal : BOOL (*Finds if two reals equal to one another within a range*)
	VAR_INPUT
		Real1 : REAL; (*First value to compare*) (* *) (*#PAR*)
		Real2 : REAL; (*Second value to compare*) (* *) (*#PAR*)
		Tolerance : REAL; (*Window or range of tollerance*) (* *) (*#PAR*)
	END_VAR
END_FUNCTION

FUNCTION IncDecCheck : BOOL (*Returns true for one cycle if 'Value' Inc/dec*)
	VAR_INPUT
		Enable : BOOL; (*Enables checking for an inc/dec. PrevVal is updated regardless*)
		Value : UDINT; (*Process variable to be checked*) (* *) (*#PAR*)
		pPrevVal : UDINT; (*Pointer to a pv of the same type as 'Value' *) (* *) (*#PAR*)
		Direction : BOOL; (*0 checks decrements, 1 checks increments*) (* *) (*#PAR*)
	END_VAR
	VAR
		prevVal : REFERENCE TO UDINT; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION AvgArr : REAL (*Returns the average of a single dimensional array of integers/floating points*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		pArr : UDINT; (*Address of the first element to be averaged*) (* *) (*#PAR*)
		datatype : USINT; (*Datatype of array (use brusDATATYPE_ prefix)*) (* *) (*#PAR*)
		numElements : UDINT; (*Number of elements to average*) (* *) (*#PAR*)
	END_VAR
	VAR
		i : UDINT; (* *) (* *) (*#OMIT*)
		sum : LREAL; (* *) (* *) (*#OMIT*)
		oneOfEach : BRUS_OneOfEach; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION_BLOCK BRUS_LinReg
	VAR_INPUT
		Execute : BOOL; (* *) (* *) (*#CMD*)
		pX : UDINT; (*Pointer to an array of REAL which contains the X data points*) (* *) (*#PAR*)
		pY : UDINT; (*Pointer to an array of REAL which contains the Y data points*) (* *) (*#PAR*)
		n : INT; (*Number of datapoints to include in the regression calculation*) (* *) (*#PAR*)
	END_VAR
	VAR_OUTPUT
		b : REAL; (*The y-offset of the linear regression equation*) (* *) (*#PAR*)
		m : REAL; (*The slope of the linear regression equation*) (* *) (*#PAR*)
		r : REAL; (*r value (correlation coefficient)*) (* *) (*#PAR*)
		rr : REAL; (*r sqared value (coefficient of determination)*) (* *) (*#PAR*)
	END_VAR
	VAR
		IS : BRUS_LinReg_IS; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION Base2Base : UDINT (*Returns the status of the function used for converting between two bases. <br> 0 - completed successfully <br> 11111 - invalid input parameter <br> 22222 - invalid character for selected base*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		InputBase : UINT; (*Base of input number, 2-36*) (* *) (*#PAR*)
		OutputBase : UINT; (*Desired output base, 2-36*) (* *) (*#PAR*)
		pInputStr : UDINT; (*Pointer to input number as a string[80]*) (* *) (*#PAR*)
		pOutputStr : UDINT; (*Pointer to where converted number will output, as a string[80]*) (* *) (*#PAR*)
	END_VAR
	VAR
		i : INT; (* *) (* *) (*#OMIT*)
		j : INT; (* *) (* *) (*#OMIT*)
		base10val : LREAL; (* *) (* *) (*#OMIT*)
		outputStr : REFERENCE TO ARRAY[0..79] OF USINT; (* *) (* *) (*#OMIT*)
		inputStr : REFERENCE TO ARRAY[0..79] OF USINT; (* *) (* *) (*#OMIT*)
		lookup : ARRAY[0..35] OF USINT := [48,49,50,51,52,53,54,55,56,57,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90]; (*0-9 and A-Z in ascii *) (* *) (*#OMIT*)
		stringLen : INT; (* *) (* *) (*#OMIT*)
		largestPow : INT; (* *) (* *) (*#OMIT*)
		lookupIdx : UDINT; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION
