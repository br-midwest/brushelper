(********************************************************
 * COPYRIGHT: B&R Industrial Automation
 ********************************************************
 * Author:	Brad Jones
 * Created:	October 14, 2016/6:09 PM 
 ********************************************************
 [program information]:
 V1.0 - Initial implementation of blink function
 ********************************************************)

(* Generates a symmetric on/off square wave with configurable period. *)
FUNCTION_BLOCK BRUS_Blink
	
	IF Enable THEN
		IS.old_clockms	:= IS.clockms;
		IS.clockms		:= TIME_TO_UDINT(clock_ms());
		
		IF NOT(Active) THEN // init			
			IF Period < T#2ms OR Period > T#1h THEN
				Error		:= TRUE;
				StatusID	:= HELPER_ERR_B_PERIOD_INVALID;
			ELSIF IS.clockms - IS.old_clockms > TIME_TO_UDINT(Period) THEN
				Error		:= TRUE;
				StatusID	:= HELPER_ERR_B_CYCLETIME_TOO_SLOW;
			ELSE
				IS.usePeriod:= TIME_TO_UDINT(Period);				
				Active		:= TRUE;
				Error		:= FALSE;
				StatusID	:= HELPER_INFO_FUB_OK;
				IS.VERSION	:= 'V1.0';
			END_IF
		ELSE // normal operation
			Out	:= (IS.clockms MOD IS.usePeriod) >= (IS.usePeriod / 2);

			// check for updates
			IF Update AND NOT(IS.old_update) THEN
				Active		:= FALSE;
			END_IF
			
			// genereate edge variables
			IS.old_update	:= Update;
		END_IF
	ELSE // disabled
		Active		:= FALSE;
		Error		:= FALSE;
		StatusID	:= HELPER_ERR_FUB_DISABLED;
		Out			:= FALSE;
	END_IF
	
END_FUNCTION_BLOCK
