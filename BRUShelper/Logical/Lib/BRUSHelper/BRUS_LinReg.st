
(* Performs a linear regression on a set of data *)
FUNCTION_BLOCK BRUS_LinReg
	
	IF Execute AND NOT(IS.oldExecute) AND n > 0 THEN
		IS.sumX		:=
		IS.sumY		:= 
		IS.sumXY	:=
		IS.sumYY	:=
		IS.sumXX	:= 0.0;
		
		FOR IS.i := 0 TO n-1 DO
			brsmemcpy(ADR(IS.X), pX+(4*IS.i), 4);
			brsmemcpy(ADR(IS.Y), pY+(4*IS.i), 4);
			
			IS.sumX		:= IS.sumX + IS.X;
			IS.sumY		:= IS.sumY + IS.Y;
			IS.sumXY	:= IS.sumXY + (IS.X*IS.Y);
			IS.sumXX	:= IS.sumXX + (IS.X*IS.X);
			IS.sumYY	:= IS.sumYY + (IS.Y*IS.Y);
		END_FOR
		
		b	:= LREAL_TO_REAL(((IS.sumY * IS.sumXX) - (IS.sumX * IS.sumXY)) / ((n * IS.sumXX) - (IS.sumX * IS.sumX)));
		m	:= LREAL_TO_REAL(((n * IS.sumXY) - (IS.sumX * IS.sumY)) / ((n * IS.sumXX) - (IS.sumX * IS.sumX)));
		r	:= LREAL_TO_REAL(((n * IS.sumXY) - (IS.sumX * IS.sumY)) / SQRT(((n * IS.sumXX) - (IS.sumX * IS.sumX)) * ((n * IS.sumYY) - (IS.sumY * IS.sumY))));
		rr	:= r * r;
	END_IF
	
	IS.oldExecute	:= Execute;
	
END_FUNCTION_BLOCK
