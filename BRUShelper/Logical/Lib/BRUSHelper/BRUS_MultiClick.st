(********************************************************
 * COPYRIGHT: B&R Industrial Automation
 ********************************************************
 * Author:	Brad Jones
 * Created:	September 16, 2016/8:38 AM 
 ********************************************************
 [program information]:
 V1.0 - one output for each single, double and triple click
 V2.0 - added output for long press (a press which is longer
		than the full input window time).
		Added version of function block to StatusID string
		after a good init.
		Changed input window range to 500-1000
 ********************************************************)

(* Detects multiple click states for a single button input with configurable input window *)
FUNCTION_BLOCK BRUS_MultiClick
	(*Errors are > 10000*)
	(*Warnings are < 10000*)
	(*No messages = 0*)	
	IF Enable THEN
		IF NOT(Active) THEN
			// check inputs
			IF InputWindow < 500 OR InputWindow > 1000 THEN
				StatusID	:= HELPER_ERR_MC_IN_WINDOW_INVALID;
				Error		:= TRUE;
			ELSE
				IS.inputWindowTime	:= UINT_TO_TIME(InputWindow);
				StatusID	:= HELPER_INFO_FUB_OK;
				Error		:= FALSE;
				Active		:= TRUE;
				IS.VERSION	:= 'V2.0';
			END_IF
		ELSE // normal operation
			// window generator
			IS.TP(IN := InputSource, PT := IS.inputWindowTime);
		
			// only if the timer window is open
			IF IS.TP.Q THEN 
				// and a positive edge is detected on input
				IF(InputSource AND NOT(IS.old_inputSource)) THEN
					IS.cntr	:= IS.cntr + 1;
					// detect a negative edge to rule out a long press
				ELSIF NOT(InputSource) AND IS.old_inputSource THEN
					IS.notLong	:= TRUE;
				END_IF
			END_IF
		
			// on the closing edge of the window, check how many presses, set outputs
			IF (NOT(IS.TP.Q) AND IS.old_tpq) THEN
				IF IS.cntr = 1 THEN
					IF IS.notLong THEN
						Single	:= TRUE;
					ELSE
						Long	:= TRUE;
					END_IF
				ELSIF IS.cntr = 2 THEN
					Double	:= TRUE;
				ELSIF IS.cntr >= 3 THEN
					Triple	:= TRUE;
				END_IF
			// after one cycle of the output, reset outputs
			ELSIF Single OR Double OR Triple OR Long THEN
				Single		:= 
				Double		:= 
				Triple		:= 
				IS.notLong	:=
				Long		:= FALSE;
				IS.cntr		:= 0;
			END_IF
			
			// check for parameter updates
			IF Update AND NOT(IS.old_update) THEN
				Active	:= FALSE;
			END_IF
		
			// edge detector variables
			IS.old_inputSource	:= InputSource;
			IS.old_tpq			:= IS.TP.Q;	
			IS.old_update		:= Update;
		END_IF
	ELSE
		StatusID	:= HELPER_ERR_FUB_DISABLED;
		Active		:= FALSE;
		Error		:= FALSE;
	END_IF
	
END_FUNCTION_BLOCK
