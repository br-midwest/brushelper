(********************************************************
 * COPYRIGHT: B&R Industrial Automation
 ********************************************************
 * Author:	Brad Jones
 * Created:	September 16, 2016/8:39 AM 
 ********************************************************
 [program information]:
 V1.0 - keeps track of time a machine spends in a condition
        without using timers. Requires permanent variable.
        Output format is configurable using MR_* constants.
 V1.1 - Added version number to status string of after good
		init.
 V2.0 - Modified I/O on fub to comply with library design
		guidelines, standard interface.
		Removed Info str output to cut down on unneccessary
		comp time. Replaced with enumerated StatusID.
 V3.0 - Removed the cycle time parameter, should be able
		to run regardless of task class. Using clock_ms().
		Added reset command to reset the counter value.
 V3.1 - Fixed bug that would add lots of time on first scan
		due to prevClock value not being populated.
 ********************************************************)

(* Keeps time that machine is in a certain state *)
FUNCTION_BLOCK BRUS_Runtime		
	(*Errors are > 10000*)
	(*Warnings are < 10000*)
	(*No messages = 0*)	
	
	IF Enable THEN
		// only operate function block if parameters are valid
		IF NOT(Active) THEN // PRE-OPERAION
			brsmemset(ADR(Runtime), 0, SIZEOF(Runtime));
				
			// check for valid parameters
			IF TimeUnit > 5 THEN
				StatusID		:= HELPER_ERR_MR_LARGE_UNIT_INVALID;
				Error			:= TRUE;
			ELSIF pRuntimeCum = 0 THEN
				StatusID		:= HELPER_ERR_MR_NULL_POINTER;
				Error			:= TRUE;
			ELSE // no errors, proceed to MAIN OPERATION			
				IF brsmemcmp(pRuntimeCum, ADR(IS.udint_zero), 4) = 0 THEN 
					StatusID	:= HELPER_WARN_MR_STARTED_FROM_ZERO;
				ELSE
					StatusID	:= HELPER_INFO_FUB_OK;
				END_IF
			
				Error			:= FALSE;
				IS.useTimeUnit	:= TimeUnit;
				RuntimeCum ACCESS pRuntimeCum;
				Active			:= TRUE;
				IS.VERSION		:= 'V3.0';
				IS.clockms		:= TIME_TO_UDINT(clock_ms());
			END_IF
		ELSE // MAIN OPERATION
			// count up in ms
			IS.old_clockms		:= IS.clockms;
			IS.clockms			:= TIME_TO_UDINT(clock_ms());
			
			IF RunCondition THEN
				IS.internalMs	:= IS.internalMs + (IS.clockms - IS.old_clockms);
				
				// generate seconds from the accumulated ms
				WHILE IS.internalMs > 1000 DO
					IS.internalMs:= IS.internalMs - 1000; // simulate MOD
					RuntimeCum	:= RuntimeCum + 1;
				END_WHILE	
			END_IF
			
			// check for updates in parameters
			IF (Update AND NOT(IS.old_update)) THEN
				Active			:= FALSE;
			ELSIF (Reset AND NOT(IS.old_reset)) THEN
				Active			:= FALSE;
				RuntimeCum		:= 0;
			END_IF
					
			// assign runtime values - only do the operations necessary
			CASE IS.useTimeUnit OF
				brusMR_SECONDS:
					Runtime.seconds	:= RuntimeCum;
			
				brusMR_MINUTES:
					Runtime.seconds	:= RuntimeCum MOD 60;
					Runtime.minutes	:= RuntimeCum / brusSEC_IN_MIN;
			
				brusMR_HOURS:
					Runtime.seconds	:= RuntimeCum MOD 60;
					Runtime.minutes	:= (RuntimeCum / brusSEC_IN_MIN) MOD 60;
					Runtime.hours	:= (RuntimeCum / brusSEC_IN_HOUR);
			
				brusMR_DAYS:
					Runtime.seconds	:= RuntimeCum MOD 60;
					Runtime.minutes	:= (RuntimeCum / brusSEC_IN_MIN) MOD 60;
					Runtime.hours	:= (RuntimeCum / brusSEC_IN_HOUR) MOD 24;
					Runtime.days	:= (RuntimeCum / brusSEC_IN_DAY);
			
				brusMR_WEEKS:
					Runtime.seconds	:= RuntimeCum MOD 60;
					Runtime.minutes	:= (RuntimeCum / brusSEC_IN_MIN) MOD 60;
					Runtime.hours	:= (RuntimeCum / brusSEC_IN_HOUR) MOD 24;
					Runtime.days	:= (RuntimeCum / brusSEC_IN_DAY) MOD 7;
					Runtime.weeks	:= (RuntimeCum / brusSEC_IN_WEEK);
			
				brusMR_YEARS:
					Runtime.seconds	:= RuntimeCum MOD 60;
					Runtime.minutes	:= (RuntimeCum / brusSEC_IN_MIN) MOD 60;
					Runtime.hours	:= (RuntimeCum / brusSEC_IN_HOUR) MOD 24;
					Runtime.days	:= (RuntimeCum / brusSEC_IN_DAY) MOD 365;
					Runtime.years	:= (RuntimeCum / brusSEC_IN_YEAR);
				ELSE // parameter out of range
					Active	:= FALSE; // send back to init
			END_CASE
			
			IS.old_update	:= Update;
			IS.old_reset	:= Reset;
		END_IF
	ELSE
		StatusID	:= HELPER_ERR_FUB_DISABLED;
		Active		:= FALSE;
		Error		:= FALSE;
		brsmemset(ADR(Runtime), 0, SIZEOF(Runtime));
	END_IF
	
END_FUNCTION_BLOCK
