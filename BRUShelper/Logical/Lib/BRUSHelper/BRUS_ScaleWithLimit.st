(********************************************************
 * COPYRIGHT: B&R Industrial Automation
 ********************************************************
 * Author:	Brandon Gunn
 * Created:	August 15, 2011 
 ********************************************************
 [program information]:
 V1.0 - Initial implementation of the function
 V2.0 - Interface changed to reflect structure of other
		function blocks in the library.
 ********************************************************)

(* Scales an Analog input to the specified range *)
FUNCTION_BLOCK BRUS_ScaleWithLimit
	
	IF Enable THEN
		IF NOT(Active) THEN
			//Check Inputs
			IF (InMax - InMin) = 0 THEN
				StatusID	:= HELPER_ERR_SWL_MAX_EQUALS_MIN;
				Error 		:= TRUE;
			ELSE
				slope := (OutMax - OutMin)/(InMax - InMin);
				constant := OutMax - (slope * InMax);
				
				StatusID	:= HELPER_INFO_FUB_OK;
				Error		:= FALSE;
				Active		:= TRUE;
				IS.VERSION	:= 'V2.0';
			END_IF
		ELSE // normal operation
			//Perform Scaling
			Out := (slope * In) + constant;
		
			//Set Range Flags
			OutOfRangeHigh := (Out > OutMax);
			OutOfRangeLow := (Out < OutMin);
		
			// check and set warnings
			HighWarning	:= Out >= HighWarnVal;
			LowWarning	:= Out <= LowWarnVal;	
		
			//Limit if desired
			IF LimitRange AND OutOfRangeHigh THEN
				Out := OutMax;
			ELSIF LimitRange AND OutOfRangeLow THEN
				Out := OutMin;
			END_IF
			
			// check for updates
			IF Update AND NOT(IS.old_update) THEN
				Active	:= FALSE;
			END_IF
			IS.old_update	:= Update;
		END_IF
		
	ELSE // disabled
		StatusID	:= HELPER_ERR_FUB_DISABLED;
		Error		:= FALSE;
		Active		:= FALSE;
		Out			:= 0.0;
	END_IF

END_FUNCTION_BLOCK