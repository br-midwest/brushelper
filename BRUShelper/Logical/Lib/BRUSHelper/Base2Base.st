
(* Converts numeric strings between bases *)
FUNCTION Base2Base
	
	IF InputBase >= 2 AND InputBase <= 36 AND OutputBase >= 2 AND OutputBase <= 36 AND pInputStr <> 0 AND pOutputStr <> 0 THEN
		inputStr ACCESS pInputStr;
		outputStr ACCESS pOutputStr;
		
		brsmemset(pOutputStr, 0, 80);// brsstrlen(pOutputStr));
		
		// convert the input number to base 10
		stringLen	:= UDINT_TO_INT(brsstrlen(pInputStr));

		FOR i := stringLen - 1 TO 0 BY -1 DO
			FOR j := 1 TO InputBase - 1 DO
				IF inputStr[i] > lookup[InputBase - 1] THEN
					Base2Base	:= 22222;
					RETURN;
				ELSIF inputStr[i] = lookup[j] THEN
					base10val	:= base10val + (lrealpow(InputBase, (stringLen - 1 - i)) * j);
				END_IF
			END_FOR
		END_FOR
		
		// convert from base 10 to output base
		FOR i := 0 TO 99 DO
			IF lrealpow(OutputBase, i) > base10val THEN
				IF i = 0 THEN
					largestPow	:= i;
				ELSE
					largestPow	:= i-1;
				END_IF
				EXIT;
			END_IF
		END_FOR
		
		FOR i := largestPow TO 0 BY -1 DO
			lookupIdx					:= TRUNC(base10val / (lrealpow(OutputBase,i)));
			outputStr[largestPow - i]	:= lookup[lookupIdx];
			base10val					:= base10val - (lrealpow(OutputBase,i) * UDINT_TO_LREAL(lookupIdx));
		END_FOR	
		
		Base2Base	:= 0; // Completed successfully
	ELSE
		Base2Base	:= 11111;
	END_IF
	
END_FUNCTION

FUNCTION lrealpow
	
	lrealpow	:= 1;	
	IF exponent > 0 THEN
		FOR i := 0 TO UINT_TO_INT(exponent - 1) DO
			lrealpow	:= lrealpow * UINT_TO_LREAL(base);
		END_FOR
	END_IF

END_FUNCTION

