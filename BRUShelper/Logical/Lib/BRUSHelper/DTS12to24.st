(********************************************************
 * COPYRIGHT: B&R Industrial Automation
 ********************************************************
 * Author:	Brad Jones
 * Created:	October 13, 2016/12:31 PM 
 ********************************************************
 [program information]:
 V1.0 - Initial implementation of the function
 ********************************************************)

(* Converts 12 hour time DTStructure back to 24 hour time DTStructure. *)
FUNCTION DTS12to24
	
	DT24	ACCESS pDTStruct24Hr;
	DT12	ACCESS pDTStruct12Hr;
	
	DT24	:= DT12;
	
	DT24.hour	:= (DT12.hour MOD 12) + (BOOL_TO_USINT(isPM) * 12);
	
END_FUNCTION