(********************************************************
 * COPYRIGHT: B&R Industrial Automation
 ********************************************************
 * Author:	Brad Jones
 * Created:	October 13, 2016/12:31 PM 
 ********************************************************
 [program information]:
 V1.0 - Initial implementation of the function
 ********************************************************)

(* Converts 24 hour time DT structure into 12 hour time DT structure. Returns TRUE for PM. *)
FUNCTION DTS24to12
	
	DT24 ACCESS pDTStruct24Hr;
	DT12 ACCESS pDTStruct12Hr;
	
	DT12	:= DT24;
	
	isPM		:= (DT24.hour >= 12);
	DT12.hour	:= DT24.hour MOD 12;
	
	IF DT12.hour = 0 THEN
		DT12.hour	:= 12;
	END_IF
	
	DTS24to12	:= isPM;
	
END_FUNCTION
