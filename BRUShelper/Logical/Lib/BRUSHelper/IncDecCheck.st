(********************************************************
 * COPYRIGHT: B&R Industrial Automation
 ********************************************************
 * Author:	Matt Adams
 * Created:	October 14, 2016/9:07 AM 
 ********************************************************
 [program information]:
 V1.0 - Initial implementation of the function
 ********************************************************)

 (* Functional increment function, returns true for 1 cycle when the value is incremented in the appropriate direction *)
FUNCTION IncDecCheck
	
	prevVal	ACCESS pPrevVal;
	
	IncDecCheck	:= (((Value > prevVal) AND Direction = 1) OR ((Value < prevVal) AND Direction = 0)) AND Enable;
	prevVal 	:= Value;
	
END_FUNCTION