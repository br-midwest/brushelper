(********************************************************
 * COPYRIGHT: B&R Industrial Automation
 ********************************************************
 * Author:	Matt Adams
 * Created:	April 04, 2014
 ********************************************************
 [program information]:
 V1.0 - Initial implementation of the function
 ********************************************************)

(* Rounds a float to a given decimal place *)
FUNCTION RoundReal
	
	IF Decimals > 0 THEN
		ExpTemp  := brmpow(10.0,Decimals);
		TempREAL := Real1 * ExpTemp;
		
		Remainder := brmmodf(TempREAL,ADR(IntPart));
		TempREAL := brmfloor(TempREAL);
		IF Remainder >= 0.5 THEN
			TempREAL := TempREAL + 1;
		END_IF
	
		ExpTemp  := brmpow(10.0,Decimals);
		RoundReal := TempREAL / ExpTemp;		
	ELSE
		RoundReal := Real1;
	END_IF
	
END_FUNCTION