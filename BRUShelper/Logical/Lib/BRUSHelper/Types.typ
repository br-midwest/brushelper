(*General*)

TYPE
	BRUS_ID_enum : 
		(
		HELPER_INFO_FUB_OK := 0,
		HELPER_WARN_MR_STARTED_FROM_ZERO := 9999,
		HELPER_ERR_MR_CYCLE_TIME_INVALID := 10001,
		HELPER_ERR_MR_LARGE_UNIT_INVALID := 10002, (*Largest unit invalid*)
		HELPER_ERR_MR_NULL_POINTER := 10003,
		HELPER_ERR_MC_IN_WINDOW_INVALID := 10010,
		HELPER_ERR_SWL_MAX_EQUALS_MIN := 10020,
		HELPER_ERR_B_PERIOD_INVALID := 10030,
		HELPER_ERR_B_CYCLETIME_TOO_SLOW := 10031,
		HELPER_ERR_FUB_DISABLED := 65534
		);
	BRUS_OneOfEach : 	STRUCT 
		mySINT : SINT;
		myUSINT : USINT;
		myINT : INT;
		myUINT : UINT;
		myDINT : DINT;
		myUDINT : UDINT;
		myREAL : REAL;
		myLREAL : LREAL;
	END_STRUCT;
END_TYPE

(*MR - machine runtime structures*)

TYPE
	BRUS_Runtime_TimeUnit_enum : 
		(
		brusMR_YEARS := 5,
		brusMR_WEEKS := 4,
		brusMR_DAYS := 3,
		brusMR_HOURS := 2,
		brusMR_MINUTES := 1,
		brusMR_SECONDS := 0
		);
	BRUS_Runtime_Times_typ : 	STRUCT 
		years : UDINT;
		weeks : UDINT;
		days : UDINT;
		hours : UDINT;
		minutes : UDINT;
		seconds : UDINT;
	END_STRUCT;
	BRUS_Runtime_IS : 	STRUCT 
		internalMs : UDINT;
		clockms : UDINT;
		old_clockms : UDINT;
		useTimeUnit : BRUS_Runtime_TimeUnit_enum;
		old_reset : BOOL;
		old_update : BOOL;
		udint_zero : UDINT;
		VERSION : STRING[10];
	END_STRUCT;
END_TYPE

(*MC - Multi-click structures*)

TYPE
	BRUS_MultiClick_IS : 	STRUCT 
		old_tpq : BOOL;
		old_inputSource : BOOL;
		old_update : BOOL;
		notLong : BOOL;
		cntr : USINT;
		TP : TP;
		inputWindowTime : TIME;
		VERSION : STRING[10];
	END_STRUCT;
END_TYPE

(*SWL - Scale with limit structures*)

TYPE
	BRUS_ScaleWithLimit_IS : 	STRUCT 
		old_update : BOOL;
		VERSION : STRING[10];
	END_STRUCT;
END_TYPE

(*B - Blink structures*)

TYPE
	BRUS_Blink_IS : 	STRUCT 
		old_clockms : UDINT;
		clockms : UDINT;
		usePeriod : UDINT;
		old_update : BOOL;
		old_enable : BOOL;
		VERSION : STRING[10];
	END_STRUCT;
	BRUS_LinReg_IS : 	STRUCT 
		oldExecute : BOOL;
		i : INT;
		sumXY : LREAL;
		sumXX : LREAL;
		sumY : LREAL;
		sumYY: LREAL;
		sumX : LREAL;
		Y : REAL;
		X : REAL;
	END_STRUCT;
END_TYPE
