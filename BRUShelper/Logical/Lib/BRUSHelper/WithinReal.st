 (********************************************************
 * COPYRIGHT: B&R Industrial Automation
 ********************************************************
 * Author:	Matt Adams
 * Created:	March 23, 2012
 ********************************************************
 [program information]:
 V1.0 - Initial implementation of the function
 ********************************************************)

(* Finds if two reals equal to one another within a range *)
FUNCTION WithinReal
	
	IF (Real1 + Tolerance > Real2) AND (Real1 - Tolerance < Real2) THEN
		WithinReal:= TRUE;
	ELSE
		WithinReal:= FALSE;
	END_IF
	
END_FUNCTION