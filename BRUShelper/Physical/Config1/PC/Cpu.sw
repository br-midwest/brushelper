﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio FileVersion="4.9"?>
<SwConfiguration CpuAddress="SL1" xmlns="http://br-automation.co.at/AS/SwConfiguration">
  <TaskClass Name="Cyclic#1" />
  <TaskClass Name="Cyclic#2" />
  <TaskClass Name="Cyclic#3" />
  <TaskClass Name="Cyclic#4">
    <Task Name="EX_BRUS_Ru" Source="Examples.EX_BRUS_Runtime.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="EX_BRUS_Mu" Source="Examples.EX_BRUS_MultiClick.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="EX_BRUS_Bl" Source="Examples.EX_BRUS_Blink.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="EX_ScaleWi" Source="Examples.EX_BRUS_ScaleWithLimit.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="EX_DTS24to" Source="Examples.EX_DTS24to12.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="EX_DTS12to" Source="Examples.EX_DTS12to24.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="EX_WithinR" Source="Examples.EX_WithinReal.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="EX_RoundRe" Source="Examples.EX_RoundReal.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="EX_IncDecC" Source="Examples.EX_IncDecCheck.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="EX_AvgArra" Source="Examples.EX_AvgArrays.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="EX_BRUS_Li" Source="Examples.EX_BRUS_LinReg.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="EX_Base2Ba" Source="Examples.EX_Base2Base.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <Libraries>
    <LibraryObject Name="BRUSHelper" Source="Lib.BRUSHelper.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="astime" Source="Libraries.astime.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="runtime" Source="Libraries.runtime.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="standard" Source="Libraries.standard.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsBrStr" Source="Libraries.AsBrStr.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsBrMath" Source="Libraries.AsBrMath.lby" Memory="UserROM" Language="binary" Debugging="true" />
  </Libraries>
</SwConfiguration>